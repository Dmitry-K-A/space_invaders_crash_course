class Settings:

    def __init__(self):
        self.screen_width = 1440
        self.screen_height = 900
        self.bg_color = (230, 230, 230)

        self.ship_limit = 1

        self.bullet_speed = 1
        self.bullet_width = 5
        self.bullet_height = 15
        self.bullet_color = (60, 169, 69)
        self.bullets_allowed = 9
        self.alien_speed = 1.0

        self.fleet_drop_speed = 250
        self.fleet_direction = 1

        self.speedup_scale = 1.25

        self.initialize_dynamic_settings()

    def initialize_dynamic_settings(self):
        """Initialize settings that change throughout the game."""
        self.ship_speed = 1.5
        self.bullet_speed = 3.0
        self.alien_speed = 1.0

        self.fleet_direction = 1  # fleet_direction of 1 represents right; -1 represents left.

    def increase_speed(self):

        self.ship_speed *= self.speedup_scale
        self.bullet_speed *= self.speedup_scale
        self.alien_speed *= self.speedup_scale


if __name__ == '__main__':  # Проверка в рабочем ли состоянии файл
    print('Class Settings')
